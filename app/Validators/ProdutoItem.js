'use strict'

class ProdutoItem {
  get rules () {
    return {
      nome: 'required',
      url_image: 'required',
      fornecedor_id: 'required',
      valor_venda: 'required',
      valor_custo: 'required',
    }
  }
  get messages () {
    return {
      'nome.required': 'O Nome do Adicional é um campo Obrigatório',
      'url_image.required': 'Insira uma imagem para o adicional.',
      'fornecedor_id.required': 'Fornecedor é um campo Obrigatório.',
      'valor_venda.required': 'O Valor de venda é um campo Obrigatório',
      'valor_custo.required': 'O Valor de custo do produto é um campo Obrigatório'
    }
  }
}

module.exports = ProdutoItem
