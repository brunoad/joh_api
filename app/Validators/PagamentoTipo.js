'use strict'

class PagamentoTipo {
  get rules () {
    return {
      nome: 'required',
      percentual_juros: 'required',
      tipo: 'required'
    }
  }
  get messages () {
    return {
      'nome.required': 'O Nome do Tipo de Pagamento é um campo Obrigatório',
      'percentual_juros.required': 'O Percentual de Juros campo Obrigatório',
      'tipo.required': 'O Tipo é um campo Obrigatório'
    }
  }
}

module.exports = PagamentoTipo
