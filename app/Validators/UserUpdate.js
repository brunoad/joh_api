'use strict'

class UserUpdate {
  get rules () {
    return {
      username: 'required',
      email: 'required|email'
    }
  }
  get messages () {
    return {
      'username.required': 'O Nome do Usuário é um campo Obrigatório',
      'email.required': 'Email é um campo Obrigatório.',
      'email.email': 'Este email não e é valido.'
    }
  }
}

module.exports = UserUpdate
