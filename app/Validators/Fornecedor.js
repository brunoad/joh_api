'use strict'

class Fornecedor {
  get rules () {
    return {
      nome: 'required',
      telefone: 'required'
    }
  }
  get messages () {
    return {
      'nome.required': 'O Nome do Fornecedor é um campo Obrigatório',
      'telefone.required': 'Telefone é um campo Obrigatório.'
    }
  }
}

module.exports = Fornecedor
