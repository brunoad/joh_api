'use strict'

class User {
  get rules () {
    return {
      username: 'required',
      email: 'required|email',
      password: 'required'
    }
  }
  get messages () {
    return {
      'username.required': 'O Nome do Usuário é um campo Obrigatório',
      'email.required': 'Email é um campo Obrigatório.',
      'email.email': 'Este email não e é valido.',
      'password.required': 'Senha é um campo Obrigatório.'
    }
  }
}

module.exports = User
