'use strict'

class Motoboy {
  get rules () {
    return {
      nome: 'required',
      telefone: 'required'
    }
  }
  get messages () {
    return {
      'nome.required': 'O Nome do Motoboy é um campo Obrigatório',
      'telefone.required': 'Telefone é um campo Obrigatório.'
    }
  }
}

module.exports = Motoboy
