'use strict'

class Cliente {
  get rules () {
    return {
      nome: 'required',
      endereco: 'required',
      telefone: 'required'
    }
  }
  get messages () {
    return {
      'nome.required': 'O Nome do Cliente é um campo Obrigatório',
      'telefone.required': 'Telefone é um campo Obrigatório.',
      'endereco.required': 'Endereço é um campo Obrigatório.'
    }
  }
}

module.exports = Cliente
