'use strict'

class Caixa {
  get rules () {
    return {
      nome: 'required'
    }
  }
  get messages () {
    return {
      'nome.required': 'O Nome do Caixa é um campo Obrigatório'
    }
  }
}

module.exports = Caixa
