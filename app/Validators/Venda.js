'use strict'

class Motoboy {
  get rules () {
    return {
      produtos: 'required'
    }
  }
  get messages () {
    return {
      'produtos.required': 'Não e possivel realizar uma venda sem Produtos',
    }
  }
}

module.exports = Motoboy
