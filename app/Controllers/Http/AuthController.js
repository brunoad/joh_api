'use strict'
const User = use('App/Models/User')

class AuthController {
  async register ({ request }) {
    const data = request.only(['username', 'email', 'password'])
    const user = await User.create(data)
    return user
  }
  async refresToken ({request, auth}) {
    const refreshToken = request.input('refresh_token')
    return await auth.generateForRefreshToken(refreshToken)
  }
  async authenticate ({ request, auth, response }) {
    const { email, password } = request.all()

    try {
      // const token = await auth
      //   .withRefreshToken()
      //   .attempt(email, password)

      if (await auth.attempt(email, password)) {
        const user = await User.query()
          .where('email',email)
          .with('recursos')
          .first()

        const permissoes = user.toJSON().recursos.map(res => res.key)
        let token = await auth.generate(user)

        response.status(200).json(Object.assign(Object.assign(user, {permissoes}), token))
      }
    } catch (e) {
      response.status(401).json({
        message: 'Usuario ou senha incorreto.',
        data: e
      })
    }
  }
}

module.exports = AuthController
