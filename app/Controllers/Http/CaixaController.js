'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Caixa = use('App/Models/Caixa')
/**
 * Resourceful controller for interacting with caixas
 */
class CaixaController {
  /**
   * Show a list of all caixas.
   * GET caixas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { page, perPage } = Object.assign({ page: 1, perPage: 10 }, request.only(['page', 'perPage']))
    const { search } = request.only(['search'])

    return await Caixa
      .query()
      .where(request.except(['page', 'perPage', 'search']))
      .orWhere(function () {
        this.where('nome', 'like', `%${search || ''}%`)
      })
      .paginate(page, perPage)
  }

  /**
   * Render a form to be used for creating a new caixa.
   * GET caixas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new caixa.
   * POST caixas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const { nome, descricao } = request.all()
    try {
      const responseData = await Caixa.create({ nome, descricao })
      response.status(200).json({
        message: `Caixa ${nome} cadastrado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Cadastrar Caixa ${nome}`,
        error: e
      })
    }
  }

  /**
   * Display a single caixa.
   * GET caixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const responseData = await Caixa.findOrFail(params.id)
      response.status(200).json({
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Buscar Caixa`,
        error: e
      })
    }
  }

  /**
   * Render a form to update an existing caixa.
   * GET caixas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update caixa details.
   * PUT or PATCH caixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const { nome, descricao } = request.all()
    try {
      const data = await Caixa.findOrFail(params.id)
      data.merge({ nome, descricao })

      const responseData = await data.save()
      response.status(200).json({
        message: `Caixa ${nome} Alterado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Alterar Caixa ${nome}`,
        error: e
      })
    }
  }

  /**
   * Delete a caixa with id.
   * DELETE caixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try {
      const data = await Caixa.findOrFail(params.id)
      const responseData = await data.delete()

      response.status(200).json({
        message: `Caixa Deletado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Deletar Caixa `,
        error: e
      })
    }
  }
}

module.exports = CaixaController
