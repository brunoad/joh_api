'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Produto = use( 'App/Models/Produto')
const Database = use('Database')
const ProdutoImage = use('App/Models/ProdutoImage')
const fs = require('fs')
const Helpers = use('Helpers')
/**
 * Resourceful controller for interacting with produtos
 */
class ProdutoController {
  /**
   * Show a list of all produtos.
   * GET produtos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const params = request.except(['search'])
    const { search } = request.only(['search'])
    // const data = Produto.query().where('produto_tipo_id', produto_tipo_id).with('itens_produto')

    const data = Produto.query().with('itens_produto').where(params)
    data.where(function () {
      this
        .where('nome', 'LIKE', `%${search || ''}%` )
        .orWhere('nome', 'LIKE', `%${search || ''}%` )
        .orWhere('nome', 'LIKE', `%${(search || '').toUpperCase()}%` )
        .orWhere('nome', 'LIKE', `%${(search || '').toLowerCase()}%` )
        .orWhere('nome', 'LIKE', `%${(search || '').replace(/\b\w/g, l => l.toUpperCase())}%` )
    })
    response.status(200).json({
      data: await data.fetch()
    })
  }

  /**
   * Render a form to be used for creating a new produto.
   * GET produtos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new produto.
   * POST produtos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const trx = await Database.beginTransaction()
    let { nome, descricao, valor_custo, valor_venda, itens_produto, produto_tipo_id, url_image } = request.all()

    itens_produto = itens_produto.map(o => o.id)

    const data = await Produto.create({ nome, descricao, valor_custo, valor_venda, produto_tipo_id, url_image }, trx)
    if (itens_produto.length) {
      data.itens_produto().attach(itens_produto, trx)
      data.itens_produto().fetch()
    }
    trx.commit()
    return data
  }

  /**
   * Display a single produto.
   * GET produtos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const data = await Produto
        .query()
        .where('id', params.id)
        .with('itens_produto')
        .first()

      response.status(200).json({
        data: data
      })
    } catch (e) {
      console.log(e)
      response.status(200).json({
        message: `Erro ao Buscar Produto`,
        error: e
      })
    }
  }

  /**
   * Render a form to update an existing produto.
   * GET produtos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update produto details.
   * PUT or PATCH produtos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    try {
      const trx = await Database.beginTransaction()
      let { nome, descricao, valor_custo, valor_venda, itens_produto, produto_tipo_id, url_image } = request.all()
      const data = await Produto.query().where('id', params.id).with('itens_produto').first()
      if (url_image !== data.url_image) {
        let imageName = data.url_image.split('/')
        imageName = imageName[imageName.length - 1]
        let imageToDelete = await ProdutoImage.findBy('url', imageName)
        if (imageToDelete != null) {
          imageToDelete.delete()
          if (fs.existsSync(Helpers.publicPath('upload/produtos/') + imageName)) {
            fs.unlinkSync(Helpers.publicPath('upload/produtos/') + imageName)
          }
        }
      }
      data.merge({ nome, descricao, valor_custo, valor_venda, produto_tipo_id, url_image })
      data.save()
      itens_produto = itens_produto.map(o => o.id)
      let ids = data.toJSON()
      ids = ids.itens_produto.map(o => o.id)

      await data
        .itens_produto()
        .detach()

      await data
        .itens_produto()
        .attach(itens_produto, trx)

      data.itens_produto = await data.itens_produto().fetch()

      trx.commit()

      response.status(200).json({
        message: 'Produto cadastrado com Sucesso.',
        data: data
      })
    } catch (e) {
      if (e.status) {
        response.status(e.status).json({
          message: e.message
        })
      } else {
        console.log(e)
        response.status(500).json({
          error: e
        })
      }
    }
  }

  /**
   * Delete a produto with id.
   * DELETE produtos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const data = await Produto.find(params.id)
    return data.delete()
  }
}

module.exports = ProdutoController
