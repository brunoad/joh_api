'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const User = use('App/Models/User')
const Database = use('Database')
/**
 * Resourceful controller for interacting with users
 */
class UserController {
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view, auth }) {
    const hasPermissionToSeeAll = (await User.query().where('id', auth.user.id).with('recursos').first()).toJSON().recursos.some(res => res.key === 'PERMISSOES')
    const { page, perPage } = Object.assign({ page: 1, perPage: 10 }, request.only(['page', 'perPage']))
    const { search } = request.only(['search'])

    if (hasPermissionToSeeAll) {
      return await User
        .query()
        .where(request.except(['page', 'perPage', 'search']))
        .orWhere(function () {
          this.where('username', 'like', `%${search || ''}%`)
        })
        .paginate(page, perPage)
    } else {
      const user = await User
        .query()
        .where('id', auth.user.id )
        .fetch()
      response.status(200).json({
        data: user
      })
    }
  }

  /**
   * Render a form to be used for creating a new user.
   * GET users/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const { username, password, email, permissoes } = request.all()
    try {
      const trx = await Database.beginTransaction()

      const usuario = await User.create({ username, password, email })

      await usuario
        .recursos()
        .attach(permissoes, trx)

      usuario.recursos = await usuario.recursos().fetch()
      const responseData = usuario.save(trx)

      trx.commit()

      response.status(200).json({
        message: `Usuario ${ username } cadastrado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      console.log(e)
      response.status(500).json({
        message: `Erro ao Cadastrar Usuario ${ username }`,
        error: e
      })
    }
  }

  /**
   * Display a single user.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const usuario = await User.query()
        .where('id',params.id)
        .with('recursos')
        .first()

      const permissoes = usuario.toJSON().recursos.map(res => res.id)
      const responseData = Object.assign(usuario.toJSON(), { permissoes })
      response.status(200).json({
        data: responseData
      })
    }
    catch (e) {
      console.log(e)
      response.status(500).json({
        message: `Erro ao Buscar Usuario`,
        error: e
      })
    }
  }

  /**
   * Render a form to update an existing user.
   * GET users/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const { username, password, email, permissoes } = request.all()
    try {
      const data = await User.query()
        .where('id', params.id)
        .with('recursos')
        .first()
      let newData = { username, password, email }
      if (!newData.password) delete newData.password

      data.merge(newData)

      await data
        .recursos()
        .detach()

      await data
        .recursos()
        .attach(permissoes)

      await data.recursos().fetch()

      const responseData = await data.save()
      response.status(200).json({
        message: `Usuario ${ username } Alterado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      console.log(e)
      response.status(500).json({
        message: `Erro ao Alterar Usuario ${ username }`,
        error: e
      })
    }
  }

  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = UserController
