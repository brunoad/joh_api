'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const ItemsProduto = use('App/Models/ItensProduto')
const ProdutoItem = use('App/Models/ProdutoItem')
/**
 * Resourceful controller for interacting with itensprodutos
 */
class ItensProdutoController {
  /**
   * Show a list of all itensprodutos.
   * GET itensprodutos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    // console.log(request)
    const { search } = request.get(['search'])
    try {
      const data = await ItemsProduto
        .query()
        .where('nome', 'like', `%${search || ''}%`)
        .fetch()
      response.status(200).json({
        data: data
      })
    } catch (e) {
      console.log(e)
      response.status(500).json({
        erro: e
      })
    }
  }
  /**
   * Create/save a new itensproduto.
   * POST itensprodutos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    let { nome, descricao, adicional, valor_custo, valor_venda, url_image, fornecedor_id } = request.all()
    try {
      const responseData = await ItemsProduto.create({ nome, descricao, adicional, valor_custo, valor_venda, url_image, fornecedor_id })
      response.status(200).json({
        message: `Adicional ${nome} cadastrado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Cadastrar Adicional ${nome}`,
        error: e
      })
    }
  }

  /**
   * Display a single itensproduto.
   * GET itensprodutos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const responseData = await ItemsProduto.findOrFail(params.id)
      response.status(200).json({
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Buscar Adicional`,
        error: e
      })
    }
  }

  /**
   * Render a form to update an existing itensproduto.
   * GET itensprodutos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update itensproduto details.
   * PUT or PATCH itensprodutos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const { nome, descricao, adicional, valor_custo, valor_venda, url_image, fornecedor_id } = request.all()
    const data = await ItemsProduto.findOrFail(params.id)
    data.merge({ nome, descricao, adicional, valor_custo, valor_venda, url_image, fornecedor_id })
    return await data.save()
  }

  /**
   * Delete a itensproduto with id.
   * DELETE itensprodutos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try {
      const data = await ItemsProduto.query().where('id', params.id).with('produtos').first()
      await data.delete()
      response.status(200).json({
        message: 'Adicional Removido com Sucesso!'
      })
    } catch (e) {
      response.status(500).json({
        message: 'Erro generico ao adicionar Adicional!',
        data: e
      })
    }
  }
}

module.exports = ItensProdutoController
