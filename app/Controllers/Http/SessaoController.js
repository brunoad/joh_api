'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Sessao = use('App/Models/Sessao')
const Caixa = use('App/Models/Caixa')
/**
 * Resourceful controller for interacting with sessaos
 */
class SessaoController {
  /**
   * Show a list of all sessaos.
   * GET sessaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async status ({ request, response, auth }) {
    return await Sessao
      .query()
      .where('user_id', auth.user.id)
      .last()
  }

  async index ({ request, response, view }) {
  }

  async abrir ({ params, response, auth }) {
    const caixa_id = params.id
    const data = {
      caixa_id: caixa_id,
      user_id: auth.user.id,
      status: 1
    }
    try {
      let caixa = await Caixa
        .query()
        .where('id', caixa_id)
        .getCount()

      if (caixa == 0) throw {
        status: 404,
        message: 'Caixa não encontrado!',
        data: caixa
      }

      const sessoesAbertas = await Sessao
        .query()
        .where(function () {
          this.where('caixa_id', caixa_id)
          this.orWhere('user_id', auth.user.id)
        })
        .andWhere('status', 1) // Aberto
        .getCount()

      if (sessoesAbertas != 0) {
        throw {
          status: 412,
          message: 'Usuário ou caixa, ja esta sendo utilizado!'
        }
      }
      const responseData = await Sessao.create(data)
      response.status(200).json({
        data: responseData,
        message: 'Caixa Aberto !',
      })
    } catch (e) {
      if (e.status) {
        response.status(e.status).json({
          message: e.message
        })
      } else {
        response.status(500).json({
          message: 'Erro ao abrir Caixa',
          error: e
        })
      }
    }
  }

  async fechar ({ params, response, auth }) {
    try {
      const data = await Sessao
        .query()
        .where('caixa_id', params.id)
        .andWhere('status', 1) //status aberto
        .andWhere('user_id', auth.user.id)
        .first()
      if (data === null) {
        throw {
          message: 'Não existe caixa aberto com esse usuario!',
          status: 412
        }
      }
      if (data.status == 0) {
        throw {
          message: 'Caixa ja esta fechado!',
          status: 412
        }
      }
      data.merge({ status: 0 })
      await data.save()
      response.status(200).json({
        message: 'Caixa Fechado!',
        data: data
      })
    } catch (e) {
      if (e.status) {
        response.status(e.status).json({
          error: e.message
        })
      } else {
        response.status(500).json({
          error: e
        })
      }
    }
  }

  /**
   * Render a form to be used for creating a new sessao.
   * GET sessaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new sessao.
   * POST sessaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single sessao.
   * GET sessaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing sessao.
   * GET sessaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update sessao details.
   * PUT or PATCH sessaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a sessao with id.
   * DELETE sessaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = SessaoController
