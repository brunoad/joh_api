'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Cliente = use('App/Models/Cliente')
/**
 * Resourceful controller for interacting with clientes
 */
class ClienteController {
  /**
   * Show a list of all clientes.
   * GET clientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { page, perPage } = Object.assign({ page: 1, perPage: 10 }, request.only(['page', 'perPage']))
    const { search } = request.only(['search'])

    return await Cliente
      .query()
      .where(request.except(['page', 'perPage', 'search']))
      .orWhere(function () {
        this.where('telefone', 'like', `%${search || ''}%`)
        this.orWhere('whatsapp', 'like', `%${search || ''}%`)
        this.orWhere('nome', 'like', `%${search || ''}%`)
      })
      .paginate(page, perPage)
  }

  /**
   * Render a form to be used for creating a new cliente.
   * GET clientes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new cliente.
   * POST clientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const { nome, endereco, complemento, referencia, telefone, whatsapp } = request.all()
    try {
      const responseData = await Cliente.create({ nome, endereco, complemento, referencia, telefone, whatsapp  })
      response.status(200).json({
        message: `Cliente ${nome} cadastrado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Cadastrar Cliente ${nome}`,
        error: e
      })
    }
  }

  /**
   * Display a single cliente.
   * GET clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const responseData = await Cliente.findOrFail(params.id)
      response.status(200).json({
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Buscar Cliente`,
        error: e
      })
    }
  }

  /**
   * Render a form to update an existing cliente.
   * GET clientes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update cliente details.
   * PUT or PATCH clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const { nome, endereco, complemento, referencia, telefone, whatsapp  } = request.all()
    try {
      const data = await Cliente.findOrFail(params.id)
      data.merge({ nome, endereco, complemento, referencia, telefone, whatsapp  })

      const responseData = await data.save()
      response.status(200).json({
        message: `Cliente ${nome} Alterado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Alterar Cliente ${nome}`,
        error: e
      })
    }
  }

  /**
   * Delete a cliente with id.
   * DELETE clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, response }) {
    try {
      const data = await Cliente.findOrFail(params.id)
      const responseData = await data.delete()

      response.status(200).json({
        message: `Cliente Deletado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Deletar Cliente `,
        error: e
      })
    }
  }
}

module.exports = ClienteController
