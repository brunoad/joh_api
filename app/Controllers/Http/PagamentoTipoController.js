'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with pagamentotipos
 */
const PagamentoTipo = use('App/Models/PagamentoTipo')
class PagamentoTipoController {
  /**
   * Show a list of all pagamentotipos.
   * GET pagamentotipos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { page, perPage } = Object.assign({ page: 1, perPage: 10 }, request.only(['page', 'perPage']))
    const { search } = request.only(['search'])

    return await PagamentoTipo
      .query()
      .where(request.except(['page', 'perPage', 'search']))
      .orWhere(function () {
        this.orWhere('nome', 'like', `%${search || ''}%`)
      })
      .paginate(page, perPage)
  }

  /**
   * Render a form to be used for creating a new pagamentotipo.
   * GET pagamentotipos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new pagamentotipo.
   * POST pagamentotipos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const { nome, percentual_juros, tipo } = request.all()
    try {
      const responseData = await PagamentoTipo.create({ nome, percentual_juros, tipo })
      response.status(200).json({
        message: `Tipo de Pagamento ${nome} cadastrado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Cadastrar Tipo de Pagamento ${nome}`,
        error: e
      })
    }
  }

  /**
   * Display a single pagamentotipo.
   * GET pagamentotipos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const responseData = await PagamentoTipo.findOrFail(params.id)
      response.status(200).json({
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Buscar Tipo de Pagamento`,
        error: e
      })
    }
  }

  /**
   * Render a form to update an existing pagamentotipo.
   * GET pagamentotipos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update pagamentotipo details.
   * PUT or PATCH pagamentotipos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const { nome, percentual_juros, tipo } = request.all()
    try {
      const data = await PagamentoTipo.findOrFail(params.id)
      data.merge({ nome, percentual_juros, tipo })

      const responseData = await data.save()
      response.status(200).json({
        message: `Tipo de Pagamento ${nome} Alterado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Alterar Tipo de Pagamento ${nome}`,
        error: e
      })
    }
  }

  /**
   * Delete a pagamentotipo with id.
   * DELETE pagamentotipos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try {
      const data = await PagamentoTipo.findOrFail(params.id)
      const responseData = await data.delete()

      response.status(200).json({
        message: `Cliente Deletado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Deletar Tipo de Pagamento `,
        error: e
      })
    }
  }
}

module.exports = PagamentoTipoController
