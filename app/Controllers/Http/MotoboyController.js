'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Motoboy = use('App/Models/Motoboy')

/**
 * Resourceful controller for interacting with motoboys
 */
class MotoboyController {
  /**
   * Show a list of all motoboys.
   * GET motoboys
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { page, perPage } = Object.assign({ page: 1, perPage: 10 }, request.only(['page', 'perPage']))
    const { search } = request.only(['search'])

    let responsedata = await Motoboy
      .query()
      .where(request.except(['page', 'perPage', 'search']))
      .orWhere(function () {
        this.where('telefone', 'like', `%${search || ''}%`)
        this.orWhere('nome', 'like', `%${search || ''}%`)
      })
      .withCount('entregas')
      .paginate(page, perPage)

    response.status(200).json(responsedata)
  }

  /**
   * Render a form to be used for creating a new motoboy.
   * GET motoboys/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new motoboy.
   * POST motoboys
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const { nome, telefone } = request.all()
    try {
      const responseData = await Motoboy.create({ nome, telefone})
      response.status(200).json({
        message: `Motoboy ${nome} cadastrado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Cadastrar Motoboy ${nome}`,
        error: e
      })
    }
  }

  /**
   * Display a single motoboy.
   * GET motoboys/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const responseData = await Motoboy.findOrFail(params.id)
      response.status(200).json({
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Buscar Motoboy`,
        error: e
      })
    }
  }

  /**
   * Render a form to update an existing motoboy.
   * GET motoboys/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update motoboy details.
   * PUT or PATCH motoboys/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const { nome, telefone } = request.all()
    try {
      const data = await Motoboy.findOrFail(params.id)
      data.merge({ nome, telefone })

      const responseData = await data.save()
      response.status(200).json({
        message: `Motoboy ${nome} Alterado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Alterar Motoboy ${nome}`,
        error: e
      })
    }
  }

  /**
   * Delete a motoboy with id.
   * DELETE motoboys/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try {
      const data = await Motoboy.findOrFail(params.id)
      const responseData = await data.delete()

      response.status(200).json({
        message: `Motoboy Deletado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Deletar Motoboy `,
        error: e
      })
    }
  }
}

module.exports = MotoboyController
