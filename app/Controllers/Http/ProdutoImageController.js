'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Helpers = use('Helpers')
const Env = use('Env')
const ProdutoImage = use('App/Models/ProdutoImage')
/**
 * Resourceful controller for interacting with produtoimages
 */
class ProdutoImageController {
  /**
   * Show a list of all produtoimages.
   * GET produtoimages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new produtoimage.
   * GET produtoimages/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new produtoimage.
   * POST produtoimages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    try {
      const image = request.file('image')
      const path = Helpers.publicPath('upload/produtos/')
      const url = new Date().getTime() + '.' + image.subtype
      const urlPath = new Date().getTime() + '.' + image.subtype
      image.move(path, {name: url})
      const data  = await ProdutoImage.create({url})
      response.status(200).json({ url: `${Env.get('HOST_URL_PUBLIC')}/upload/produtos/${url}` })
    } catch (e) {
      response.status(500).json({
        message: 'Erro de ao fazer upload',
        data: e
      })
    }
  }

  /**
   * Display a single produtoimage.
   * GET produtoimages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing produtoimage.
   * GET produtoimages/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update produtoimage details.
   * PUT or PATCH produtoimages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a produtoimage with id.
   * DELETE produtoimages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ProdutoImageController
