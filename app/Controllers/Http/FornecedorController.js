'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Fornecedor = use('App/Models/Fornecedor')
/**
 * Resourceful controller for interacting with fornecedors
 */
class FornecedorController {
  /**
   * Show a list of all fornecedors.
   * GET fornecedors
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { page, perPage } = Object.assign({ page: 1, perPage: 10 }, request.only(['page', 'perPage']))
    const { search } = request.only(['search'])

    return await Fornecedor
      .query()
      .where(request.except(['page', 'perPage', 'search']))
      .orWhere(function () {
        this.orWhere('nome', 'like', `%${search || ''}%`)
      })
      .paginate(page, perPage)
  }

  /**
   * Render a form to be used for creating a new fornecedor.
   * GET fornecedors/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new fornecedor.
   * POST fornecedors
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const { nome, descricao, endereco, telefone } = request.all()
    try {
      const responseData = await Fornecedor.create({ nome, descricao, endereco, telefone })
      response.status(200).json({
        message: `Fornecedor ${nome} cadastrado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Cadastrar Fornecedor ${nome}`,
        error: e
      })
    }
  }

  /**
   * Display a single fornecedor.
   * GET fornecedors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const responseData = await Fornecedor.findOrFail(params.id)
      response.status(200).json({
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Buscar Fornecedor`,
        error: e
      })
    }
  }

  /**
   * Render a form to update an existing fornecedor.
   * GET fornecedors/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update fornecedor details.
   * PUT or PATCH fornecedors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const { nome, descricao, endereco, telefone } = request.all()
    try {
      const data = await Fornecedor.findOrFail(params.id)
      data.merge({ nome, descricao, endereco, telefone })

      const responseData = await data.save()
      response.status(200).json({
        message: `Fornecedor ${nome} Alterado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Alterar Fornecedor ${nome}`,
        error: e
      })
    }
  }

  /**
   * Delete a fornecedor with id.
   * DELETE fornecedors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try {
      const data = await Fornecedor.findOrFail(params.id)
      const responseData = await data.delete()

      response.status(200).json({
        message: `Fornecedor Deletado com Sucesso !`,
        data: responseData
      })
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao Deletar Fornecedor `,
        error: e
      })
    }
  }
}

module.exports = FornecedorController
