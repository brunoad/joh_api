'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Database = use('Database')
const Motoboy = use('App/Models/Motoboy')
const Cliente = use('App/Models/Cliente')
const Produtos = use('App/Models/Produto')
const Sessao = use('App/Models/Sessao')
const Movimentacoes = use('App/Models/Movimentacao')
const Venda = use('App/Models/Venda')
const ProdutoVendaAdicional = use('App/Models/ProdutoVendaAdicional')
const ItensProduto = use('App/Models/ItensProduto')
const PagamentoTipo = use('App/Models/PagamentoTipo')
/**
 * Resourceful controller for interacting with vendas
 */
class VendaController {
  /**
   * Show a list of all vendas.
   * GET vendas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { page, perPage } = Object.assign({ page: 1, perPage: 10 }, request.only(['page', 'perPage']))
    const vendas = await Venda
      .query()
      .where(request.except(['page', 'perPage', 'search', 'hasMotoboy']))
      .orderBy('created_at', 'desc')
      .andWhere(function () {
        const {hasMotoboy} = request.all()
        if (hasMotoboy) {
          this.whereNot({motoboy_id: null})
        }
      })
      .with('cliente')
      .with('motoboy')
      .paginate(page, perPage)

    const vendasObj = vendas.toJSON()
    const movimentacaoIds = vendasObj.data
      .map(res => res.movimentacao_id)

    const vendasId = vendasObj.data.map(res => res.id)

    const produtos = await ProdutoVendaAdicional
      .query()
      .whereIn('vendas_id', vendasId)
      .with('produtos')
      .with('adicional')
      .with('vendas')
      .fetch()

    const movimentacoes = await Movimentacoes.query()
      .whereIn('id', movimentacaoIds)
      .with('sessao.caixa')
      .with('sessao.user')
      .fetch()

    const movimentacoesArr = movimentacoes.toJSON()

    const data = vendas.toJSON().data
      .map(venda => {
        venda.movimento = movimentacoesArr.find(movimento => movimento.id == venda.movimentacao_id)
        venda.adicionais = produtos.toJSON().find(produto => produto.vendas_id == venda.id)
        return venda
      })

    response.status(200).json(Object.assign(vendasObj, {data}))
  }

  /**
   * Render a form to be used for creating a new venda.
   * GET vendas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new venda.
   * POST vendas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response, auth }) {
    const { motoboy_id, cliente_id, produtos, valor_acrescimo, pagamento_tipo } = request.all()
    const status = 1

    try {
      // Validar Se existe Sessao aberta com usuario
      const sessao = await Sessao
        .query()
        .where('user_id', auth.user.id)
        .andWhere('status', 1) // status aberto
        .last()

      if (sessao === null){
        throw {
          message: 'Não existe caixa aberto com esse usuario!',
          status: 412
        }
      }

      // Validar Se existe produtos
      if (produtos.length === 0){
        throw {
          message: 'Não e possivel realizar uma venda sem Produto!',
          status: 412
        }
      }

      const checkValores = produtos.every(res => res.hasOwnProperty('valor_venda'))

      if (!checkValores){
        throw {
          message: 'Erro, não foi informado valor de um ou varios Produtos!',
          status: 412
        }
      }

      const produto_ids = produtos.map(res => res.id)

      // Verifica se existe Motoboy
      const checkMotoboy = await Motoboy
        .query()
        .where('id', motoboy_id)
        .first()

      if ((motoboy_id !== null) && (checkMotoboy === null)){
        throw {
          message: 'Não existe Motoboy com este id!',
          status: 412
        }
      }

      // Verifica se existe Cliente
      const checkCliente = await Cliente
        .query()
        .where('id', cliente_id)
        .first()

      if ((cliente_id !== null) && (checkCliente === null)){
        throw {
          message: 'Não existe Cliente com este id!',
          status: 412
        }
      }

      const total_produtos = produtos.reduce((total, produto) => total + produto.valor_venda ,0)

      // const { total_produtos } = await Produtos
      //   .query()
      //   .whereIn('id', produto_ids)
      //   .sum('valor_venda as total_produtos')
      //   .first()

      const adicionais_ids = produtos.map(res => {
        return res.adicionais.map(adicional => adicional.id)
      }).reduce((acumuladora, atual) => acumuladora.concat(atual),[])

      const { total_adicionais } = await ItensProduto
        .query()
        .whereIn('id', adicionais_ids)
        .sum('valor_venda as total_adicionais')
        .first()

      let valor_total = valor_acrescimo + total_produtos + total_adicionais
      const pagamento = await PagamentoTipo.findOrFail(pagamento_tipo)
      const percentual_juros = (pagamento.percentual_juros/100) + 1
      valor_total = parseFloat((valor_total * percentual_juros).toFixed(2))

      const movimentoFinanceiro = {
        valor: valor_total,
        tipo: 1, // tipo entrada
        sessao_id: sessao.id,
        origem: 'Venda'
      }

      const trx = await Database.beginTransaction()

      const movimentacao = await Movimentacoes.create(movimentoFinanceiro, trx)

      const data = await Venda
        .create({
          status,
          motoboy_id,
          cliente_id,
          valor_acrescimo,
          pagamento_tipo
        }, trx)

      await data
        .movimentacao()
        .associate(movimentacao, trx)

      await trx.commit()

      const adicionais =  produtos.map(res => {
        let adicionais = res.adicionais.map(adicional => ({
          itens_produtos_id: adicional.id,
          produtos_id: res.id,
          vendas_id: data.id
        }))
        return adicionais
      }).reduce((acumuladora, atual) => acumuladora.concat(atual),[])


      await ProdutoVendaAdicional.createMany(adicionais)

      const responseData = await Venda.query()
        .where('id', data.id)
        .with('adicionais')
        .first()

      if (adicionais.length && !responseData.length ) {
        data.delete()
        movimentacao.delete()
      }

      response
        .status(200)
        .json({
          message: 'Pedido realizado com Sucesso.',
          data: responseData
        })
    } catch (e) {
      if (e.status) {
        response.status(e.status).json({
          message: e.message
        })
      } else {
        console.log(e)
        response.status(500).json({
          error: e
        })
      }
    }
  }

  /**
   * Display a single venda.
   * GET vendas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing venda.
   * GET vendas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update venda details.
   * PUT or PATCH vendas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }
  async mudarStatus ({ params, request, response }) {
    const { status } = request.all()
    try {
      const venda = await Venda.findOrFail(params.id)
      await venda.merge({ status })

      const responseData = await venda.save()

      response.status(200).json(responseData)
    } catch (e) {
      response.status(500).json({
        message: `Erro ao mudar status de Venda`,
        error: e
      })
    }
  }

  /**
   * Delete a venda with id.
   * DELETE vendas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = VendaController
