'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Sessao = use('App/Models/Sessao')
const Venda = use('App/Models/Venda')
const PagamentoTipo = use('App/Models/PagamentoTipo')
/**
 * Resourceful controller for interacting with balancos
 */
class BalancoController {
  /**
   * Show a list of all balancos.
   * GET balancos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { page, perPage } = Object.assign({ page: 1, perPage: 10 }, request.only(['page', 'perPage']))
    const { search } = request.only(['search'])

    try {
      const dataSessao = await Sessao.query()
        .where(request.except(['page', 'perPage', 'search']))
        .with('movimento')
        .with('caixa')
        .with('user')
        .orderBy('id', 'desc')
        .paginate(page, perPage)

      // return dataSessao
      const data = dataSessao.toJSON().data.map(res => {
        res.movimento_total = res.movimento.reduce((total, mv) => total + mv.valor, 0)
        return res
      })

      const responseData = Object.assign(dataSessao.toJSON(), { data })

      response.status(200).json(responseData)
    }
    catch (e) {
      response.status(500).json({
        message: `Erro ao lista`,
        error: e
      })
    }
  }

  /**
   * Render a form to be used for creating a new balanco.
   * GET balancos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new balanco.
   * POST balancos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single balanco.
   * GET balancos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    try {
      const dataSessao = await Sessao.query()
        .where('id', params.id)
        .with('movimento')
        .with('caixa')
        .with('user')
        .first()

      if (dataSessao == null) {
        throw {
          message: 'Sessão de trabalho não foi encontrada!',
          status: 404
        }
      }

      // Pagamento tipo 1 = cartão
      const movimentoIds = dataSessao.toJSON().movimento.map(res => res.id)
      const vendasMovimento = await Venda.query()
        .whereIn('movimentacao_id', movimentoIds)
        .andWhere(function () {
          this.whereNot('status', 0)
        })
        .fetch()

      const pag = await PagamentoTipo.query()
        .with('vendas.movimentacao', builder => {
          builder.whereIn('movimentacao_id', movimentoIds)
        })
        .fetch()
      const tiposPagamentosValor = pag.toJSON().map(res => {
        res.vendas = res.vendas.map(vRes => {
          vRes.movimento = dataSessao.toJSON().movimento.find(movimento => movimento.id === vRes.movimentacao_id)
          return vRes
        })
          return res
      })

      // return rTurn

      const tipo_pagamento_valor = tiposPagamentosValor.map(res => {
        const valor = res.vendas.reduce((total, venda) => {
          const vendaValor = venda.movimento ? venda.movimento.valor : 0
          return total + (venda.status ? vendaValor : 0)
        }, 0)
        res.valor = valor
        delete res.vendas
        return res
      })

      const movimentosIdEntregas = vendasMovimento.toJSON()
        .filter((res) => res.motoboy_id != null)
        .map(res => res.movimentacao_id)

      const vendasRealizadas = await Venda.query()
        .whereIn('movimentacao_id', movimentoIds)
        .andWhere(function () {
          this.where('status', 0)
        })
        .fetch()

      const qtd_canceladas = vendasRealizadas.toJSON().length

      const valor_entrada_entregas = dataSessao.toJSON().movimento.reduce((total, mv) => movimentosIdEntregas.some(id => id === mv.id) ? total + mv.valor : total, 0)
      const qtd_vendas = dataSessao.toJSON().movimento.length
      const qtd_entregas = movimentosIdEntregas.length

      // const totalMovimento = dataSessao.toJSON().movimento.reduce((total, mv) => total + mv.valor, 0)

      const responseData = Object.assign(dataSessao.toJSON(), { valor_entrada_entregas, qtd_vendas, qtd_entregas, tipo_pagamento_valor, qtd_canceladas })
      response.status(200).json({
        data: responseData
      })
    } catch (e) {
      if (e.status) {
        response.status(e.status).json({
          message: e.message
        })
      } else {
        console.log(e)
        response.status(500).json({
          error: e
        })
      }
    }
  }

  /**
   * Render a form to update an existing balanco.
   * GET balancos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update balanco details.
   * PUT or PATCH balancos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a balanco with id.
   * DELETE balancos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = BalancoController
