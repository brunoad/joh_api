'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const ProdutoTipo = use('App/Models/ProdutoTipo')
/**
 * Resourceful controller for interacting with produtotipos
 */
class ProdutoTipoController {
  /**
   * Show a list of all produtotipos.
   * GET produtotipos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    return ProdutoTipo.all()
  }

  /**
   * Render a form to be used for creating a new produtotipo.
   * GET produtotipos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new produtotipo.
   * POST produtotipos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const data = request.all()
    return ProdutoTipo.create(data)
  }

  /**
   * Display a single produtotipo.
   * GET produtotipos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    return await ProdutoTipo.findOrFail(params.id)
  }

  /**
   * Render a form to update an existing produtotipo.
   * GET produtotipos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update produtotipo details.
   * PUT or PATCH produtotipos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    let data = request.except(['id'])
    let tipo = await ProdutoTipo.find(params.id)

    tipo.merge(data)
    return await tipo.save()
  }

  /**
   * Delete a produtotipo with id.
   * DELETE produtotipos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    let data = await ProdutoTipo.find(params.id)
    data.delete()
  }
}

module.exports = ProdutoTipoController
