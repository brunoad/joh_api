'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Movimentacao extends Model {
  venda () {
    return this.hasMany('App/Models/Venda', 'movimento_id', 'id')
  }
  sessao () {
    return this.belongsTo('App/Models/Sessao')
  }
}

module.exports = Movimentacao
