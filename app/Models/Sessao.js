'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Sessao extends Model {
  caixa () {
    return this.belongsTo('App/Models/Caixa')
  }
  movimento () {
    return this.hasMany('App/Models/Movimentacao')
  }
  user () {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Sessao
