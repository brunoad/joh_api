'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Motoboy extends Model {
  entregas () {
    return this.hasMany('App/Models/Venda')
  }
}

module.exports = Motoboy
