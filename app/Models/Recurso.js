'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Recurso extends Model {
  usuarios () {
    return this.belongsToMany('App/Models/Recurso').pivotTable('usuario_recursos')
  }
}

module.exports = Recurso
