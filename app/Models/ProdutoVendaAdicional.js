'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProdutoVendaAdicional extends Model {
  produtos () {
    return this.hasMany('App/Models/Produto', 'produto_id', 'id')
  }
  vendas () {
    return this.hasMany('App/Models/Venda', 'vendas_id', 'id')
  }
  adicional () {
    return this.hasMany('App/Models/ItensProduto', 'itens_produtos_id', 'id')
  }
}

module.exports = ProdutoVendaAdicional
