'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PagamentoTipo extends Model {
  vendas () {
    return this.hasMany('App/Models/Venda', 'id', 'pagamento_tipo')
  }
}

module.exports = PagamentoTipo
