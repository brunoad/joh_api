'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ItensProduto extends Model {
  produtos () {
    return this.belongsToMany('App/Models/Produto').pivotTable('produto_itens')
  }
}

module.exports = ItensProduto
