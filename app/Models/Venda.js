'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Venda extends Model {
  movimentacao () {
    return this.belongsTo('App/Models/Movimentacao', 'movimentacao_id ', 'id')
  }
  adicionais () {
    return this.belongsToMany('App/Models/ItensProduto', 'itens_produtos_id', 'id')
      .pivotTable('produto_venda_adicionals')
  }
  cliente () {
    return this.belongsTo('App/Models/Cliente')
  }
  motoboy () {
    return this.belongsTo('App/Models/Motoboy', 'motoboy_id', 'id')
  }
  pagamentoTipo () {
    return this.belongsTo('App/Models/PagamentoTipo', 'pagamento_tipo', 'id')
  }
}

module.exports = Venda
