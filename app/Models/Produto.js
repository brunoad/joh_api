'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Produto extends Model {
  produto_tipo () {
    return this.belongsTo('App/Models/ProdutoTipo')
  }
  itens_produto () {
    return this.belongsToMany('App/Models/ItensProduto').pivotTable('produto_itens')
  }
  vendas () {
    return this.belongsToMany('App/Models/Vendas').pivotTable('produto_vendas')
  }
}
module.exports = Produto
