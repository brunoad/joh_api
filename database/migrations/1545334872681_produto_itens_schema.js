'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoItensSchema extends Schema {
  up () {
    this.create('produto_itens', (table) => {
      table.increments()
      table
        .integer('produto_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('produtos')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

      table
        .integer('itens_produto_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('itens_produtos')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

      table.timestamps()
    })
  }

  down () {
    this.drop('produto_itens')
  }
}

module.exports = ProdutoItensSchema
