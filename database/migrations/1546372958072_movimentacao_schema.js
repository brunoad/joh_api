'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MovimentacaoSchema extends Schema {
  up () {
    this.create('movimentacaos', (table) => {
      table.increments()
      table.string('origem')
      table
        .integer('sessao_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('sessaos')
      table
        .integer('tipo')
        .default(1)
        .unsigned()
        .notNullable()
      table
        .float('valor')
        .default(0)
        .unsigned()
        .notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('movimentacaos')
  }
}

module.exports = MovimentacaoSchema
