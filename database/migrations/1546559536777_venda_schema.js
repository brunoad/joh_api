'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VendaSchema extends Schema {
  up () {
    this.table('vendas', (table) => {
      table
        .integer('motoboy_id')
        .unsigned()
        .references('id')
        .inTable('motoboys')
      table.integer('pagamento_tipo')
      table.float('valor_acrescimo')
      // alter table
    })
  }

  down () {
    this.table('vendas', (table) => {
      table.dropColumn('motoboy_id')
      table.dropColumn('pagamento_tipo')
      table.dropColumn('valor_acrescimo')
      // reverse alternations
    })
  }
}

module.exports = VendaSchema
