'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentoTipoSchema extends Schema {
  up () {
    this.create('pagamento_tipos', (table) => {
      table.increments()
      table.string('nome')
      table.float('percentual_juros').default(0)
      table.integer('tipo').default(0) // 0 DInheiro, 1 cartão
      table.timestamps()
    })
  }

  down () {
    this.drop('pagamento_tipos')
  }
}

module.exports = PagamentoTipoSchema
