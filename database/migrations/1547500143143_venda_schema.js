'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VendaSchema extends Schema {
  up () {
    this.table('vendas', (table) => {
      table.integer('status')
        .default(1)
    })
  }

  down () {
    this.table('vendas', (table) => {
      table.dropColumn('status')
      // reverse alternations
    })
  }
}

module.exports = VendaSchema
