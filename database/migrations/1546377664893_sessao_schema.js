'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SessaoSchema extends Schema {
  up () {
    this.table('sessaos', (table) => {
      table
        .integer('caixa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('caixas')
      // alter table
    })
  }

  down () {
    this.table('sessaos', (table) => {
      table.dropColumn('caixa_id')
      // reverse alternations
    })
  }
}

module.exports = SessaoSchema
