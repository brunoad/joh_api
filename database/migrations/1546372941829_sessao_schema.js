'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SessaoSchema extends Schema {
  up () {
    this.create('sessaos', (table) => {
      table.increments()
      table
        .integer('status', 1)
        .default(1)
        .notNullable()
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('sessaos')
  }
}

module.exports = SessaoSchema
