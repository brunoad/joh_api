'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsuarioRecursoSchema extends Schema {
  up () {
    this.create('usuario_recursos', (table) => {
      table.increments()
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

      table
        .integer('recurso_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('recursos')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

      table.timestamps()
    })
  }

  down () {
    this.drop('usuario_recursos')
  }
}

module.exports = UsuarioRecursoSchema
