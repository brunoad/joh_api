'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoVendaSchema extends Schema {
  up () {
    this.create('produto_vendas', (table) => {
      table.increments()
      table
        .integer('produto_id')
        .unsigned()

      table
        .foreign('produto_id')
        .references('produtos.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('produto_vendas')
  }
}

module.exports = ProdutoVendaSchema
