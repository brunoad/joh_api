'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoTipoSchema extends Schema {
  up () {
    this.create('produto_tipos', (table) => {
      table.increments()
      table
        .string('nome', 60)
        .notNullable()
      table.string('descricao', 240)
      table.timestamps()
    })
  }

  down () {
    this.drop('produto_tipos')
  }
}

module.exports = ProdutoTipoSchema
