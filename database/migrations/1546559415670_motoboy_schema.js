'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MotoboySchema extends Schema {
  up () {
    this.create('motoboys', (table) => {
      table.increments()
      table
        .string('nome', 60)
        .notNullable()
      table
        .string('telefone', 11)
      table.timestamps()
    })
  }

  down () {
    this.drop('motoboys')
  }
}

module.exports = MotoboySchema
