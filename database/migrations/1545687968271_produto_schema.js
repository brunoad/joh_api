'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoSchema extends Schema {
  up () {
    this.table('produtos', (table) => {
      table.string('url_image')
      // alter table
    })
  }

  down () {
    this.table('produtos', (table) => {
      // reverse alternations
      table.dropColumn('url_image');
    })
  }
}

module.exports = ProdutoSchema
