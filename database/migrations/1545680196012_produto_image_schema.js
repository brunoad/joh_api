'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoImageSchema extends Schema {
  up () {
    this.create('produto_images', (table) => {
      table.increments()
      table
        .string('url')
        .notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('produto_images')
  }
}

module.exports = ProdutoImageSchema
