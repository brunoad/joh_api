'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ItensProdutoSchema extends Schema {
  up () {
    this.table('itens_produtos', (table) => {
      table.string('url_image')
      // alter table
    })
  }

  down () {
    this.table('itens_produtos', (table) => {
      table.dropColumn('url_image');
      // reverse alternations
    })
  }
}

module.exports = ItensProdutoSchema
