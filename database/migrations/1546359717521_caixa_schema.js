'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CaixaSchema extends Schema {
  up () {
    this.create('caixas', (table) => {
      table.increments()
      table
        .string('nome', 60)
        .notNullable()
      table
        .string('descricao', 240)
      table
      table.timestamps()
    })
  }

  down () {
    this.drop('caixas')
  }
}

module.exports = CaixaSchema
