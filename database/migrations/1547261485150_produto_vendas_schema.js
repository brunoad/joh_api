'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoVendasSchema extends Schema {
  up () {
    this.table('produto_vendas', (table) => {
      table
        .integer('vendas_id')
        .unsigned()

      table
        .foreign('vendas_id')
        .references('vendas.id')
      // alter table
    })
  }

  down () {
    this.table('produto_vendas', (table) => {
      table.dropColumn('vendas_id')
      // reverse alternations
    })
  }
}

module.exports = ProdutoVendasSchema
