'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClienteSchema extends Schema {
  up () {
    this.create('clientes', (table) => {
      table.increments()
      table
        .string('nome', 60)
        .notNullable()
      table
        .string('endereco', 240)
        .notNullable()
      table
        .string('complemento', 240)
      table
        .string('referencia', 240)
      table
        .string('telefone', 11)
        .notNullable()
      table
        .string('whatsapp', 11)
      table.timestamps()
    })
  }

  down () {
    this.drop('clientes')
  }
}

module.exports = ClienteSchema
