'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ItensProdutoSchema extends Schema {
  up () {
    this.create('itens_produtos', (table) => {
      table.increments()
      table.string('nome', 60)
      table.string('descricao', 240)
      table
        .integer('adicional', 1)
        .default(0)
      table
        .float('valor_custo')
        .default(0)
      table
        .float('valor_venda')
        .default(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('itens_produtos')
  }
}

module.exports = ItensProdutoSchema
