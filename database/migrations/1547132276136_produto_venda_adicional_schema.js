'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoVendaAdicionalSchema extends Schema {
  up () {
    this.create('produto_venda_adicionals', (table) => {
      table.increments()
      table
        .integer('produtos_id')
        .unsigned()
      table
        .integer('itens_produtos_id')
        .unsigned()
      table
        .integer('vendas_id')
        .unsigned()

      table
        .foreign('produtos_id')
        .references('produtos.id')
      table
        .foreign('itens_produtos_id')
        .references('itens_produtos.id')
      table
        .foreign('vendas_id')
        .references('vendas.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('produto_venda_adicionals')
  }
}

module.exports = ProdutoVendaAdicionalSchema
