'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MovimentacaoSchema extends Schema {
  up () {
    this.table('movimentacaos', (table) => {
      table
        .integer('status')
        .default(0)
      // alter table
    })
  }

  down () {
    this.table('movimentacaos', (table) => {
      table.dropColumn('status')
      // reverse alternations
    })
  }
}

module.exports = MovimentacaoSchema
