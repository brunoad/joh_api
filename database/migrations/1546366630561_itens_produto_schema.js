'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ItensProdutoSchema extends Schema {
  up () {
    this.table('itens_produtos', (table) => {
      // alter table
      table
        .integer('fornecedor_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('fornecedors')
    })
  }

  down () {
    this.table('itens_produtos', (table) => {
      table.dropColumn('fornecedor_id')
      // reverse alternations
    })
  }
}

module.exports = ItensProdutoSchema
