'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoSchema extends Schema {
  up () {
    this.create('produtos', (table) => {
      table.increments()
      table
        .string('nome', 60)
        .notNullable()
      table.string('descricao', 240)
      table
        .float('valor_custo')
        .default(0)
      table
        .float('valor_venda')
        .default(0)
      table
        .integer('produto_tipo_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('produto_tipos')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('produtos')
  }
}

module.exports = ProdutoSchema
