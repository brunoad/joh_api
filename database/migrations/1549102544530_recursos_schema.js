'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RecursosSchema extends Schema {
  up () {
    this.create('recursos', (table) => {
      table.increments()
      table.string('key')
        .notNullable()
      table.string('descricao')
        .notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('recursos')
  }
}

module.exports = RecursosSchema
