'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FornecedorSchema extends Schema {
  up () {
    this.create('fornecedors', (table) => {
      table.increments()
      table
        .string('nome', 60)
        .notNullable()
      table
        .string('descricao', 240)
      table
        .string('endereco', 240)
      table
        .string('telefone', 11)
        .notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('fornecedors')
  }
}

module.exports = FornecedorSchema
