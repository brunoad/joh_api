'use strict'

/*
|--------------------------------------------------------------------------
| RecursoSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Recurso = use ('App/Models/Recurso')

const recursosData = [
  { key: 'PERMISSOES', descricao: 'Permissões' },
  { key: 'PEDIDO_NOVO', descricao: 'Pedido: Novo' },
  { key: 'PEDIDO_ESPERA', descricao: 'Pedido: Espera' },
  { key: 'PEDIDO_FINALIZADO', descricao: 'Pedido: Finalizado' },
  { key: 'PEDIDO_ENTREGA', descricao: 'Pedido: Entrega' },
  { key: 'CAIXA_INFO', descricao: 'Pedido: Informação de Caixa' },
  { key: 'CAIXA_FLUXO', descricao: 'Caixa: Cadastros' },
  { key: 'MOTOBOY', descricao: 'Motoboy' },
  { key: 'FORNECEDOR', descricao: 'Fornecedores' },
  { key: 'USUARIO', descricao: 'Usuário' },
  { key: 'MEIO_PAGAMENTO', descricao: 'Meio de Pagamento' },
  { key: 'CAIXA', descricao: 'Caixa' },
  { key: 'CLIENTE', descricao: 'Cliente' },
  { key: 'ADICIONAL', descricao: 'Adicional' },
  { key: 'BEBIDA', descricao: 'Bebida' },
  { key: 'DOCE', descricao: 'Doce' },
  { key: 'LANCHE', descricao: 'Lanche' },
]

class RecursoSeeder {
  async run () {
    await Recurso.createMany(recursosData)
  }
}

module.exports = RecursoSeeder

