'use strict'

/*
|--------------------------------------------------------------------------
| ProdutoTipoSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/
const defaultData = [
  {
    nome: 'Lanche',
    descricao: 'Lanche'
  },
  {
    nome: 'Bebida',
    descricao: 'Bebida'
  },
  {
    nome: 'Doces/Sobremesas',
    descricao: 'Doces/Sobremesas'
  }
]
/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const ProdutoTipo = use ('App/Models/ProdutoTipo')
class ProdutoTipoSeeder {
  async run () {
    await ProdutoTipo.createMany(defaultData)
  }
}

module.exports = ProdutoTipoSeeder
