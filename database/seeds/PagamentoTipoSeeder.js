'use strict'

/*
|--------------------------------------------------------------------------
| PagamentoTipoSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const dinheiro = {
  nome: 'Dinheiro',
  percentual_juros: 0,
  tipo: 0
}
const credito = {
  nome: 'Crédito',
  percentual_juros: 5,
  tipo: 1
}
const debito = {
  nome: 'Débito',
  percentual_juros: 2,
  tipo: 1
}
const PagamentoTipo = use('App/Models/PagamentoTipo')
class PagamentoTipoSeeder {
  async run () {
    const tiposPagamentos = [dinheiro, credito, debito]
    await PagamentoTipo.createMany(tiposPagamentos)
  }
}

module.exports = PagamentoTipoSeeder
