'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use ('App/Models/User')

class UserSeeder {
  async run () {
    await User.create({
      username: 'Joh',
      email: 'joh@adm.com',
      password: '123@123'
    })
  }
}

module.exports = UserSeeder
