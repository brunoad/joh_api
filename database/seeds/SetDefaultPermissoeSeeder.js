'use strict'

/*
|--------------------------------------------------------------------------
| SetDefaultPermissoeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')
const Recursos = use('App/Models/Recurso')
class SetDefaultPermissoeSeeder {
  async run () {
    const recursos = (await Recursos.all())
      .toJSON()
      .map(res => res.id)
    const adm = await User.find(1)
    adm.recursos()
      .detach()

    adm.recursos()
      .attach(recursos)

    await adm.save()
  }
}

module.exports = SetDefaultPermissoeSeeder
