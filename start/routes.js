'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');
Route.post('/register', 'AuthController.register');
Route.post('/authenticate', 'AuthController.authenticate');
Route.post('/refres-token', 'AuthController.refresToken');

// ------------------------------------------------------------------
//   Produto Tipos
// ------------------------------------------------------------------
Route.group(() => {
  Route
    .resource('produto-tipos', 'ProdutoTipoController')
    .apiOnly()
})
  .middleware('auth');
// ------------------------------------------------------------------
// ------------------------------------------------------------------

// ------------------------------------------------------------------
//   Produto
// ------------------------------------------------------------------
Route.group(() => {
  Route.get('produtos', 'ProdutoController.index')
  Route.post('produtos', 'ProdutoController.store').validator('Produto')
  Route.get('produtos/:id', 'ProdutoController.show')
  Route.put('produtos/:id', 'ProdutoController.update').validator('Produto')
  Route.delete('produtos/:id', 'ProdutoController.destroy')
})
  .middleware('auth');
// ------------------------------------------------------------------
// ------------------------------------------------------------------

// ------------------------------------------------------------------
//   Produto - Image
// ------------------------------------------------------------------

Route.post('produto-imagems', 'ProdutoImageController.store')

// ------------------------------------------------------------------
// ------------------------------------------------------------------

// ------------------------------------------------------------------
//   Items Produto
// ------------------------------------------------------------------
Route.group(() => {
  Route.get('adicionais', 'ItensProdutoController.index')
  Route.post('adicionais', 'ItensProdutoController.store').validator('ProdutoItem')
  Route.get('adicionais/:id', 'ItensProdutoController.show')
  Route.put('adicionais/:id', 'ItensProdutoController.update').validator('ProdutoItem')
  Route.delete('adicionais/:id', 'ItensProdutoController.destroy')
})
  .middleware('auth');
// ------------------------------------------------------------------
// ------------------------------------------------------------------


// ------------------------------------------------------------------
//   Items Clijente
// ------------------------------------------------------------------
Route.group(() => {
  Route.get('clientes', 'ClienteController.index')
  Route.post('clientes', 'ClienteController.store').validator('Cliente')
  Route.get('clientes/:id', 'ClienteController.show')
  Route.put('clientes/:id', 'ClienteController.update').validator('Cliente')
  Route.delete('clientes/:id', 'ClienteController.destroy')
})
  .middleware('auth');
// ------------------------------------------------------------------
// ------------------------------------------------------------------



// ------------------------------------------------------------------
//   Items Fornecedores
// ------------------------------------------------------------------
Route.group(() => {
  Route.get('fornecedores', 'FornecedorController.index')
  Route.post('fornecedores', 'FornecedorController.store').validator('Fornecedor')
  Route.get('fornecedores/:id', 'FornecedorController.show')
  Route.put('fornecedores/:id', 'FornecedorController.update').validator('Fornecedor')
  Route.delete('fornecedores/:id', 'FornecedorController.destroy')
})
  .middleware('auth');
// ------------------------------------------------------------------
// ------------------------------------------------------------------


// ------------------------------------------------------------------
//   Items Balanço
// ------------------------------------------------------------------
Route.group(() => {
  Route.get('balancos', 'BalancoController.index')
  Route.get('balancos/:id', 'BalancoController.show')
}).middleware('auth');
// ------------------------------------------------------------------
// ------------------------------------------------------------------


// ------------------------------------------------------------------
//   Items Caixas
// ------------------------------------------------------------------
Route.group(() => {
  Route.get('status-caixa', 'SessaoController.status')
  Route.post('caixas/:id/abrir', 'SessaoController.abrir')
  Route.post('caixas/:id/fechar', 'SessaoController.fechar')
  // ==============================================
  Route.get('caixas', 'CaixaController.index')
  Route.post('caixas', 'CaixaController.store').validator('Caixa')
  Route.get('caixas/:id', 'CaixaController.show')
  Route.put('caixas/:id', 'CaixaController.update').validator('Caixa')
  Route.delete('caixas/:id', 'CaixaController.destroy')
}).middleware('auth');
// ------------------------------------------------------------------
// ------------------------------------------------------------------

// ------------------------------------------------------------------
//   Items Motoboys
// ------------------------------------------------------------------
Route.group(() => {
  Route.get('motoboys', 'MotoboyController.index')
  Route.post('motoboys', 'MotoboyController.store').validator('Caixa')
  Route.get('motoboys/:id', 'MotoboyController.show')
  Route.put('motoboys/:id', 'MotoboyController.update').validator('Caixa')
  Route.delete('motoboys/:id', 'MotoboyController.destroy')
}).middleware('auth')
// ------------------------------------------------------------------
// ------------------------------------------------------------------

// ------------------------------------------------------------------
//   Items Vendas
// ------------------------------------------------------------------
Route.group(() => {
  Route.put('vendas/:id/mudar-status', 'VendaController.mudarStatus')
  Route.get('vendas', 'VendaController.index')
  Route.post('vendas', 'VendaController.store').validator('Venda')
}).middleware('auth')
// ------------------------------------------------------------------
// ------------------------------------------------------------------

// ------------------------------------------------------------------
//   User
// ------------------------------------------------------------------
Route.group(() => {
  Route.get('users', 'UserController.index')
  Route.post('users', 'UserController.store').validator('User')
  Route.get('users/:id', 'UserController.show')
  Route.put('users/:id', 'UserController.update').validator('UserUpdate')
  Route.delete('users/:id', 'UserController.destroy')
}).middleware('auth')
// ------------------------------------------------------------------
// ------------------------------------------------------------------

// ------------------------------------------------------------------
//   Pagamento Tipo
// ------------------------------------------------------------------
Route.group(() => {

  Route.get('pagamento-tipos', 'PagamentoTipoController.index')
  Route.post('pagamento-tipos', 'PagamentoTipoController.store').validator('PagamentoTipo')
  Route.get('pagamento-tipos/:id', 'PagamentoTipoController.show')
  Route.put('pagamento-tipos/:id', 'PagamentoTipoController.update').validator('PagamentoTipo')
  Route.delete('pagamento-tipos/:id', 'PagamentoTipoController.destroy')
}).middleware('auth')
// ------------------------------------------------------------------
// ------------------------------------------------------------------

// ------------------------------------------------------------------
//   Pagamento Tipo
// ------------------------------------------------------------------
Route.group(() => {
  Route.get('recursos', 'RecursoController.index')
}).middleware('auth')
// ------------------------------------------------------------------
// ------------------------------------------------------------------
